# README #

1/3スケールのJR200風小物のstlファイルです。 
スイッチ類、I/Oポート等は省略しています。 
 
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 
元ファイルはAUTODESK Fusion360です。 


***

# 実機情報

## メーカ
- Nationnal
- 松下電器産業

## 発売時期
- JR-200 1982年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/JR_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF)#JR-200)
- [ボクたちが愛した、想い出のパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1069058.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_jr200/raw/bd4e4be9841b939725eb2ebf82ece8032f45b02f/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_jr200/raw/bd4e4be9841b939725eb2ebf82ece8032f45b02f/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_jr200/raw/bd4e4be9841b939725eb2ebf82ece8032f45b02f/ExampleImage.png)
